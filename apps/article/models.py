
from django.db import models
from django.contrib.auth import get_user_model
from pytz import unicode
from ckeditor.fields import RichTextField

User = get_user_model()


class CategoryArticle(models.Model):

    name = models.CharField(max_length=200)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '1. Категорія статей'
        verbose_name_plural = '1. Категорія статей'


class Article(models.Model):

    category = models.ForeignKey(CategoryArticle,
                                 blank=True, null=True, on_delete=models.SET_NULL)

    img = models.ImageField('img', null=True, blank=True, upload_to='info_img')

    title = models.CharField(max_length=400, null=True, blank=True)

    description = models.CharField(max_length=400, null=True, blank=True)

    keywords = models.CharField(max_length=400, null=True, blank=True)

    name = models.CharField(max_length=400)

    small_description = models.CharField(max_length=400, null=True, blank=True)

    article = RichTextField(null=True, blank=True)

    visible = models.BooleanField(default=False)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '2. Статті'
        verbose_name_plural = '2. Статті'