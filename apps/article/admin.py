
from django.contrib import admin


from apps.article.models import CategoryArticle, Article


class CategoryArticleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


admin.site.register(CategoryArticle, CategoryArticleAdmin)
admin.site.register(Article, ArticleAdmin)
