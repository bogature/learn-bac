from django.contrib.auth import get_user_model
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status, mixins, generics
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.article.models import Article
from apps.article.pagination import ArticlePagination
from apps.article.serializers import ArticleSerializer
from apps.courses.models import \
    Category, SubCategory, Topic, Test, Answer, Task, InformationTest, HistoryTest, HelpTask, VideoLesson, Courses, \
    SectionCourses, ElementSection
from apps.courses.pagination import CategoryPagination, SubCategoryPagination, TopicPagination, TestPagination, \
    InformationTestPagination, HistoryTesPagination, VideoLessonPagination, CoursesPagination, SectionCoursesPagination, \
    ElementSectionPagination
from apps.courses.serializers import CategorySerializer, SubCategorySerializer, TopicSerializer, TestSerializer, \
    TestSmallSerializer, AnswerSerializer, TaskSerializer, TaskSmallSerializer, InformationTestSerializer, \
    HistoryTestSerializer, HistoryTestSmallSerializer, HelpTaskSerializer, VideoLessonSerializer, CoursesSerializer, \
    SectionCoursesSerializer, ElementSectionSerializer, ElementSectionSmallSerializer, CoursesSmallSerializer, \
    SectionCoursesSmallSerializer

User = get_user_model()


class GetListAllArticle(generics.ListAPIView):
    serializer_class = ArticleSerializer
    pagination_class = ArticlePagination

    def get_queryset(self):
        return Article.objects.filter(visible=True).order_by('-id')


class ItemArticle(APIView):

    def get_object(self, pk):
        try:
            return Article.objects.get(pk=pk)
        except Article.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ArticleSerializer(snippet)
        return Response(serializer.data)
