
from django.urls import path

from apps.article.views import GetListAllArticle, ItemArticle


urlpatterns = [

    path('all/', GetListAllArticle.as_view()),
    path('info/<int:pk>/', ItemArticle.as_view()),

]