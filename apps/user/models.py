from django.db import models
from django.contrib.auth import get_user_model
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.shortcuts import get_object_or_404
from pytz import unicode

User = get_user_model()


class Level(models.Model):

    name = models.CharField(max_length=200)

    min_mark = models.IntegerField()

    max_mark = models.IntegerField()

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '2. Уровень'
        verbose_name_plural = '2. Уровень'


class Profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user')

    sponsor = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE, related_name='sponsor')

    name = models.CharField(max_length=200, null=True, blank=True)

    first_name = models.CharField(max_length=250, null=True, blank=True)

    last_name = models.CharField(max_length=250, null=True, blank=True)

    location = models.CharField(max_length=200, null=True, blank=True)

    status = models.CharField(max_length=200, null=True, blank=True)

    phone = models.CharField(max_length=200, null=True, blank=True)

    email = models.CharField(max_length=200, null=True, blank=True)

    level = models.ForeignKey(Level, null=True, blank=True, on_delete=models.CASCADE)

    mark = models.IntegerField(default=0)

    pro = models.BooleanField(default=False)

    activated_pro = models.DateTimeField(null=True, blank=True)

    teacher = models.BooleanField(default=False)

    request_teacher = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % self.user

    class Meta:
        verbose_name = '1. Профиль'
        verbose_name_plural = '1. Профиль'


class Friends(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='friend_user')

    profile = models.OneToOneField(Profile, null=True, blank=True,
                                   on_delete=models.CASCADE, related_name='friend_user')

    friends = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return unicode(self.user)

    class Meta:
        verbose_name = '3. Друзі'
        verbose_name_plural = '3. Друзі'


class TeacherRequest(models.Model):

    user = models.ForeignKey(User, null=True, blank=True,
                             on_delete=models.CASCADE, related_name='teach_user')

    done = models.BooleanField(default=False)

    def __str__(self):
        return unicode(self.user)

    class Meta:
        verbose_name = '4. Запит на зміну статусу (Учитель)'
        verbose_name_plural = '4. Запит на зміну статусу (Учитель)'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        Friends.objects.create(user=instance)


@receiver(post_save, sender=Profile)
def update_profile(sender, instance, created, **kwargs):
    levels = Level.objects.all()
    for level in levels:
        if level.min_mark <= instance.mark <= level.max_mark:
            Profile.objects.filter(id=instance.user.pk).update(level=get_object_or_404(Level, pk=level.pk))
