# Generated by Django 3.0.4 on 2020-05-19 11:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0012_teacherrequest'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='request_teacher',
            field=models.BooleanField(default=False),
        ),
    ]
