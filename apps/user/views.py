
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.http import Http404
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.user.models import Profile, Friends
from apps.user.pagination import FriendsPagination
from apps.user.serializers import UserSerializer, ProfileSerializer, \
    ProfilePutSerializer, FriendsSerializer, TeacherRequestSerializer

User = get_user_model()


class ProfileUpdateViews(APIView):

    def get_object(self, pk):
        return Profile.objects.get(pk=pk)

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ProfileSerializer(snippet)
        return Response(serializer.data)

    def patch(self, request, pk):

        profile = self.get_object(pk)
        serializer = ProfilePutSerializer(profile, data=request.data, partial=True)

        if request.user == profile.user:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        else:
            Response('No authentication user', status=status.HTTP_400_BAD_REQUEST)


class GetUser(APIView):

    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = UserSerializer(snippet)
        return Response(serializer.data)


class GetUserToken(APIView):

    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        snippet = self.get_object(request.user.pk)
        serializer = UserSerializer(snippet)
        return Response(serializer.data)


class ProfileViews(APIView):

    def get_object(self, pk):
        try:
            return Profile.objects.get(pk=pk)
        except Profile.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ProfileSerializer(snippet)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ProfileSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetListFriendsUser(generics.ListAPIView):
    serializer_class = FriendsSerializer
    pagination_class = FriendsPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Friends.objects.filter(user_id=pk).order_by('-id')


class TeacherCreateViews(APIView):

    def post(self, request, format=None):
        serializer = TeacherRequestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user_id=request.user.pk)

            Profile.objects.filter(user_id=request.user.pk).update(request_teacher=True)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddFriendsUserViews(APIView):

    def post(self, request, format=None):
        if User.objects.filter(username=request.data.get('username')).count() > 0:
            user = User.objects.get(username=request.data.get('username'))
            Friends.objects.get(user=request.user).friends.add(user)

            return Response('ok', status=status.HTTP_201_CREATED)
        else:
            return Response('no user', status=status.HTTP_400_BAD_REQUEST)


class RemoveFriendsUserViews(APIView):

    def post(self, request, format=None):
        if User.objects.filter(username=request.data.get('username')).count() > 0:
            user = User.objects.get(username=request.data.get('username'))
            Friends.objects.get(user=request.user).friends.remove(user)

            return Response('ok', status=status.HTTP_201_CREATED)
        else:
            return Response('no user', status=status.HTTP_400_BAD_REQUEST)


class UpdatePasswordViews(APIView):

    def post(self, request, format=None):

        password = request.data.get('password')
        new_password = request.data.get('new')

        user = User.objects.filter(username=request.user.username)[0]

        if user.check_password(password):
            user.password = make_password(new_password)
            user.save()
            return Response('yes', status=status.HTTP_201_CREATED)
        else:
            return Response('no', status=status.HTTP_400_BAD_REQUEST)

