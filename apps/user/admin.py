
from django.contrib import admin

from apps.user.models import *


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user',)


class LevelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'min_mark', 'max_mark',)


class FriendsTaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'user',)


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Level, LevelAdmin)
admin.site.register(Friends, FriendsTaskAdmin)
admin.site.register(TeacherRequest)
