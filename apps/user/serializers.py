
from django.contrib.auth import get_user_model
from rest_framework import serializers
from apps.user.models import Profile, Level, Friends, TeacherRequest

User = get_user_model()


class UserProfileSmallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['first_name', 'last_name']


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'email']


class LevelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Level
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True, many=False)

    level = LevelSerializer(read_only=True, many=False)

    class Meta:
        model = Profile
        fields = '__all__'


class ProfilePutSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ['first_name', 'last_name', 'location',
                  'phone', 'email', 'level', 'mark', 'teacher']


class ProfileFriendsSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True, many=False)

    class Meta:
        model = Profile
        fields = ['id', 'user']


class UserFriendsSerializer(serializers.ModelSerializer):
    user = ProfileFriendsSerializer(read_only=True, many=False)

    class Meta:
        model = Profile
        fields = ['id', 'user']


class FriendsSerializer(serializers.ModelSerializer):

    friends = UserSerializer(read_only=True, many=True)

    class Meta:
        model = Friends
        fields = ['friends']


class TeacherRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeacherRequest
        fields = '__all__'
