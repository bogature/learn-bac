
from django.urls import path

from apps.user.views import *

urlpatterns = [

    path('<int:pk>/', GetUser.as_view()),
    path('', GetUserToken.as_view()),
    path('profile/<int:pk>/', ProfileUpdateViews.as_view()),
    path('status/teacher/create/', TeacherCreateViews.as_view()),
    path('friends/list/<int:pk>/', GetListFriendsUser.as_view()),
    path('add/friends/', AddFriendsUserViews.as_view()),
    path('remove/friends/', RemoveFriendsUserViews.as_view()),
    path('update/password/', UpdatePasswordViews.as_view()),

]
