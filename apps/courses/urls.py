
from django.contrib import admin
from django.urls import path, include

from apps.courses.views import *

urlpatterns = [

    path('video/lesson/list/<int:pk>/', GetListVideoLesson.as_view()),
    path('category/list/', GetListAllCategory.as_view()),
    path('sub/category/list/<int:pk>/', GetListSubCategory.as_view()),
    path('topic/list/<int:pk>/', GetListTopic.as_view()),
    path('test/list/<int:pk>/', GetListTest.as_view()),
    path('info/test/list/<int:pk>/', GetListInformationTest.as_view()),
    path('history/test/list/<int:pk>/', GetListHistoryTest.as_view()),
    path('help/test/list/<int:pk>/', GetListHelpTest.as_view()),
    path('history/test/', HistoryTestViews.as_view()),
    path('answer/', AnswerViews.as_view()),
    path('test/', TestViews.as_view()),
    path('task/', TaskViews.as_view()),
    path('topic/<int:pk>/', ItemTopicViews.as_view()),
    path('patch/test/<int:pk>/', PatchTestViews.as_view()),
    path('patch/task/<int:pk>/', PatchTaskViews.as_view()),
    path('courses/list/', GetListAllCourses.as_view()),
    path('section/list/<int:pk>/', GetListSectionCourses.as_view()),
    path('element/section/list/<int:pk>/', GetListElementSection.as_view()),
    path('create/courses/', CoursesViews.as_view()),
    path('create/section/', SectionViews.as_view()),
    path('create/element/section/', ElementSectionViews.as_view()),
    path('delete/element/section/', DeleteElementSectionViews.as_view()),
    path('search/', SearchCourseViews.as_view()),

]