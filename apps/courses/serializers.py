
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from apps.courses.models import \
    Category, SubCategory, Topic, Answer, Test, Task, InformationTest, HistoryTest, HelpTask, VideoLesson, Courses, \
    SectionCourses, ElementSection
from apps.user.serializers import UserSerializer


class CoursesSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True, many=False)

    class Meta:
        model = Courses
        fields = '__all__'


class SectionCoursesSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = SectionCourses
        fields = '__all__'

class SectionCoursesSerializer(serializers.ModelSerializer):

    class Meta:
        model = SectionCourses
        fields = '__all__'


class ElementSectionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ElementSection
        fields = '__all__'


class ElementSectionSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = ElementSection
        fields = '__all__'


class CoursesSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = Courses
        fields = '__all__'


class VideoLessonSerializer(serializers.ModelSerializer):

    class Meta:
        model = VideoLesson
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    logo = serializers.ImageField()

    class Meta:
        model = Category
        fields = '__all__'


class SubCategorySerializer(serializers.ModelSerializer):

    category = CategorySerializer(read_only=True, many=False)

    class Meta:
        model = SubCategory
        fields = '__all__'


class TopicSerializer(serializers.ModelSerializer):

    sub_category = SubCategorySerializer(read_only=True, many=False)

    class Meta:
        model = Topic
        fields = '__all__'


class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answer
        fields = '__all__'


class TaskSerializer(serializers.ModelSerializer):

    others_answer = AnswerSerializer(read_only=True, many=True)
    correct_answer = AnswerSerializer(read_only=True, many=True)

    class Meta:
        model = Task
        fields = '__all__'


class TaskSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = '__all__'


class TestSerializer(serializers.ModelSerializer):

    topic = SubCategorySerializer(read_only=True, many=False)
    task = TaskSerializer(read_only=True, many=True)

    class Meta:
        model = Test
        fields = '__all__'


class TestSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = Test
        fields = '__all__'


class InformationTestSerializer(serializers.ModelSerializer):

    class Meta:
        model = InformationTest
        fields = '__all__'


class HistoryTestSerializer(serializers.ModelSerializer):

    topic = TopicSerializer(read_only=True, many=False)

    class Meta:
        model = HistoryTest
        fields = '__all__'


class HelpTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = HelpTask
        fields = '__all__'


class HistoryTestSmallSerializer(serializers.ModelSerializer):

    class Meta:
        model = HistoryTest
        fields = ['id', 'user', 'topic', 'count_correct_answer', 'count_all_answer', 'percent']