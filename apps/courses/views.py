from django.contrib.auth import get_user_model
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status, mixins, generics
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.courses.models import \
    Category, SubCategory, Topic, Test, Answer, Task, InformationTest, HistoryTest, HelpTask, VideoLesson, Courses, \
    SectionCourses, ElementSection
from apps.courses.pagination import CategoryPagination, SubCategoryPagination, TopicPagination, TestPagination, \
    InformationTestPagination, HistoryTesPagination, VideoLessonPagination, CoursesPagination, SectionCoursesPagination, \
    ElementSectionPagination
from apps.courses.serializers import CategorySerializer, SubCategorySerializer, TopicSerializer, TestSerializer, \
    TestSmallSerializer, AnswerSerializer, TaskSerializer, TaskSmallSerializer, InformationTestSerializer, \
    HistoryTestSerializer, HistoryTestSmallSerializer, HelpTaskSerializer, VideoLessonSerializer, CoursesSerializer, \
    SectionCoursesSerializer, ElementSectionSerializer, ElementSectionSmallSerializer, CoursesSmallSerializer, \
    SectionCoursesSmallSerializer
from apps.user.models import Profile

User = get_user_model()


class GetListAllCourses(generics.ListAPIView):
    serializer_class = CoursesSerializer
    pagination_class = CoursesPagination

    def get_queryset(self):
        return Courses.objects.all()


class GetListSectionCourses(generics.ListAPIView):
    serializer_class = SectionCoursesSerializer
    pagination_class = SectionCoursesPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return SectionCourses.objects.filter(courses_id=pk)


class GetListElementSection(generics.ListAPIView):
    serializer_class = ElementSectionSerializer
    pagination_class = ElementSectionPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return ElementSection.objects.filter(section_id=pk)


class CoursesViews(APIView):

    def get_object(self, pk):
        try:
            return Courses.objects.get(pk=pk)
        except Courses.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        serializer = CoursesSmallSerializer(data=request.data)

        profile = Profile.objects.filter(user_id=request.user.id)[0]

        if profile.teacher:
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('You are not a Teacher', status=status.HTTP_400_BAD_REQUEST)


class SectionViews(APIView):

    def get_object(self, pk):
        try:
            return SectionCourses.objects.get(pk=pk)
        except SectionCourses.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        serializer = SectionCoursesSmallSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ElementSectionViews(APIView):

    def get_object(self, pk):
        try:
            return ElementSection.objects.get(pk=pk)
        except ElementSection.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        serializer = ElementSectionSmallSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeleteElementSectionViews(APIView):

    def get_object(self, pk):
        try:
            return ElementSection.objects.get(pk=pk)
        except ElementSection.DoesNotExist:
            raise Http404

    def post(self, request, format=None):

        count_el = ElementSection.objects.filter(pk=request.data.get('element')).count()
        if(count_el > 0):
            ElementSection.objects.filter(pk=request.data.get('element')).delete()
            return Response("Valid delete", status=status.HTTP_201_CREATED)
        else:
            return Response('No element', status=status.HTTP_400_BAD_REQUEST)


class GetListAllCategory(generics.ListAPIView):
    serializer_class = CategorySerializer
    pagination_class = CategoryPagination

    def get_queryset(self):
        return Category.objects.all()


class GetListVideoLesson(generics.ListAPIView):
    serializer_class = VideoLessonSerializer
    pagination_class = VideoLessonPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return VideoLesson.objects.filter(sub_category_id=pk)
        # return VideoLesson.objects.all()


class GetListSubCategory(generics.ListAPIView):
    serializer_class = SubCategorySerializer
    pagination_class = SubCategoryPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return SubCategory.objects.filter(category_id=pk)


class GetListTopic(generics.ListAPIView):
    serializer_class = TopicSerializer
    pagination_class = TopicPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Topic.objects.filter(sub_category_id=pk)


class GetListTest(generics.ListAPIView):
    serializer_class = TestSerializer
    pagination_class = TestPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Test.objects.filter(topic_id=pk)


class GetListInformationTest(generics.ListAPIView):
    serializer_class = InformationTestSerializer

    # pagination_class = InformationTestPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return InformationTest.objects.filter(topic_id=pk)


class GetListHistoryTest(generics.ListAPIView):
    serializer_class = HistoryTestSerializer
    pagination_class = HistoryTesPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return HistoryTest.objects.filter(user_id=pk).order_by('-id')


class GetListHelpTest(generics.ListAPIView):
    serializer_class = HelpTaskSerializer

    # pagination_class = HistoryTesPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return HelpTask.objects.filter(test_id=pk)


class HistoryTestViews(APIView):

    def get_object(self, pk):
        try:
            return HistoryTest.objects.get(pk=pk)
        except HistoryTest.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        serializer = HistoryTestSmallSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(user=get_object_or_404(User, username=request.user))

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnswerViews(APIView):

    def get_object(self, pk):
        try:
            return Answer.objects.get(pk=pk)
        except Answer.DoesNotExist:
            raise Http404

    def post(self, request, format=None):

        data = request.data

        if data.get('img') is not None:
            flag = True
        else:
            flag = False

        if flag:
            serializer = AnswerSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data.get('id'), status=status.HTTP_201_CREATED)
        else:

            answer = Answer.objects.filter(text=str(data.get('text')))
            if answer.__len__() == 0:
                serializer = AnswerSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                    print(serializer.data.get('id'))
                    return Response(serializer.data.get('id'), status=status.HTTP_201_CREATED)
            else:
                return Response(answer[0].id, status=status.HTTP_201_CREATED)

        return Response('error', status=status.HTTP_400_BAD_REQUEST)


class TestViews(APIView):

    def get_object(self, pk):
        try:
            return Test.objects.get(pk=pk)
        except Test.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        serializer = TestSmallSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TaskViews(APIView):

    def get_object(self, pk):
        try:
            return Task.objects.get(pk=pk)
        except Task.DoesNotExist:
            raise Http404

    def post(self, request, format=None):
        serializer = TaskSmallSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PatchTaskViews(APIView):

    def get_object(self, pk):
        try:
            return Task.objects.get(pk=pk)
        except Task.DoesNotExist:
            raise Http404

    def patch(self, request, pk):
        testmodel_object = self.get_object(pk)
        serializer = TaskSmallSerializer(testmodel_object, data=request.data,
                                         partial=True)  # set partial=True to update a data partially
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class PatchTestViews(APIView):

    def get_object(self, pk):
        try:
            return Test.objects.get(pk=pk)
        except Test.DoesNotExist:
            raise Http404

    def patch(self, request, pk):
        testmodel_object = self.get_object(pk)
        serializer = TestSmallSerializer(testmodel_object, data=request.data,
                                         partial=True)  # set partial=True to update a data partially
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)


class ItemTopicViews(APIView):

    def get_object(self, pk):
        try:
            return Topic.objects.get(pk=pk)
        except Topic.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = TopicSerializer(snippet)
        return Response(serializer.data)


class SearchCourseViews(ListModelMixin, GenericAPIView):

    serializer_class = CoursesSerializer
    pagination_class = CoursesPagination

    def get_queryset(self):
        obj = []

        search = self.request.GET.get('search')

        if search is not None and search != '':
            courses = Courses.objects.all()
            for elem in courses:
                if search.lower() in elem.name.lower():
                    obj.append(elem)

        return obj

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

