# Generated by Django 3.0.4 on 2020-04-10 17:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0040_test_open_answer_test'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='exam',
            field=models.BooleanField(default=False),
        ),
    ]
