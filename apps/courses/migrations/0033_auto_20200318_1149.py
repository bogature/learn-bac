# Generated by Django 3.0.4 on 2020-03-18 11:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0032_auto_20200315_0919'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='informationtest',
            options={'verbose_name': '8. Інформація для тестів(ТЕОРІЯ)', 'verbose_name_plural': '8. Інформація для тестів(ТЕОРІЯ)'},
        ),
        migrations.AddField(
            model_name='informationtest',
            name='img',
            field=models.ImageField(blank=True, null=True, upload_to='info_img', verbose_name='img'),
        ),
    ]
