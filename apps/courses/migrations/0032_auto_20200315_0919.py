# Generated by Django 3.0.4 on 2020-03-15 09:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0031_historytest'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='historytest',
            options={'verbose_name': '9. Історія прогресу тестів', 'verbose_name_plural': '9. Історія прогресу тестів'},
        ),
    ]
