# Generated by Django 3.0.4 on 2020-03-26 15:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0036_auto_20200324_1131'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='helptask',
            name='task',
        ),
        migrations.AddField(
            model_name='helptask',
            name='test',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='courses.Test'),
        ),
    ]
