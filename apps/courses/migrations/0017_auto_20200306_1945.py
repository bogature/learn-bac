# Generated by Django 2.2.6 on 2020-03-06 19:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0016_auto_20200306_0809'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': '6. Запитання',
                'verbose_name_plural': '6. Запитання',
            },
        ),
        migrations.CreateModel(
            name='Conformity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answers', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='answers', to='courses.Answer')),
                ('question', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='question', to='courses.Question')),
            ],
            options={
                'verbose_name': '7. Відповідності',
                'verbose_name_plural': '7. Відповідності',
            },
        ),
        migrations.AddField(
            model_name='test',
            name='conformity',
            field=models.ManyToManyField(blank=True, null=True, related_name='conformity', to='courses.Conformity'),
        ),
    ]
