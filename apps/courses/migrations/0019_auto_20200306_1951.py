# Generated by Django 2.2.6 on 2020-03-06 19:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0018_auto_20200306_1950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='correct_answer',
            field=models.ManyToManyField(blank=True, related_name='task_correct_answer', to='courses.Answer'),
        ),
        migrations.AlterField(
            model_name='task',
            name='others_answer',
            field=models.ManyToManyField(blank=True, related_name='task_others_answer', to='courses.Answer'),
        ),
    ]
