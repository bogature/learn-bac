
from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from pytz import unicode

User = get_user_model()


class Category(models.Model):

    logo = models.ImageField('logo', null=True, blank=True, upload_to='logo')

    name = models.CharField(max_length=200)

    count_sub_category = models.IntegerField(default=0)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '1. Категорія'
        verbose_name_plural = '1. Категорія'


class SubCategory(models.Model):

    category = models.ForeignKey(Category,  blank=True, null=True, on_delete=models.SET_NULL)

    name = models.CharField(max_length=200)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '2. Під категорія'
        verbose_name_plural = '2. Під категорія'


class Topic(models.Model):

    sub_category = models.ForeignKey(SubCategory,  blank=True, null=True, on_delete=models.SET_NULL)

    name = models.CharField(max_length=200)

    open = models.BooleanField(default=True)

    password_topic = models.CharField(max_length=200, null=True, blank=True)

    exam = models.BooleanField(default=False)

    max_prize = models.FloatField(default=0)

    theories = models.BooleanField(default=False)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '3. Тема'
        verbose_name_plural = '3. Теми'


class Answer(models.Model):

    img = models.ImageField('img', null=True, blank=True, upload_to='img')

    text = models.TextField(null=True, blank=True)

    def __str__(self):
        return unicode(self.text)

    class Meta:
        verbose_name = '5. Відповіді'
        verbose_name_plural = '5. Відповіді'


class Task(models.Model):

    img = models.ImageField('img', null=True, blank=True, upload_to='task_img')

    question = models.TextField(null=True, blank=True)

    open_answer = models.BooleanField(default=False)

    help = models.TextField(null=True, blank=True)

    prize = models.FloatField(default=0)

    others_answer = models.ManyToManyField(Answer, blank=True, related_name='task_others_answer')

    correct_answer = models.ManyToManyField(Answer, blank=True, related_name='task_correct_answer')

    def __str__(self):
        return unicode(self.question)

    class Meta:
        verbose_name = '7. Завдання'
        verbose_name_plural = '7. Завдання'


class Test(models.Model):

    general_question = models.TextField(null=True, blank=True)

    topic = models.ForeignKey(Topic,  blank=True, null=True, on_delete=models.SET_NULL)

    task = models.ManyToManyField(Task, blank=True, related_name='task')

    open_answer_test = models.BooleanField(default=False)

    count_answer = models.IntegerField(default=0)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '4. Тест'
        verbose_name_plural = '4. Тест'


class InformationTest(models.Model):

    topic = models.ForeignKey(Topic, null=True, blank=True, on_delete=models.SET_NULL)

    img = models.ImageField('img', null=True, blank=True, upload_to='info_img')

    text = models.TextField(blank=True, null=True)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '8. Інформація для тестів(ТЕОРІЯ)'
        verbose_name_plural = '8. Інформація для тестів(ТЕОРІЯ)'


class HistoryTest(models.Model):

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)

    topic = models.ForeignKey(Topic, null=True, blank=True, on_delete=models.SET_NULL)

    count_correct_answer = models.IntegerField(default=0)

    count_all_answer = models.IntegerField(default=0)

    percent = models.IntegerField(default=0)

    data_time = models.DateField(default=timezone.now)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '9. Історія прогресу тестів'
        verbose_name_plural = '9. Історія прогресу тестів'


class HelpTask(models.Model):

    test = models.ForeignKey(Test, null=True, blank=True, on_delete=models.SET_NULL)

    img = models.ImageField('img', null=True, blank=True, upload_to='info_img')

    text = models.TextField(blank=True, null=True)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '10. Допомога (Підказка)'
        verbose_name_plural = '10. Допомога (Підказка)'


class VideoLesson(models.Model):

    name = models.CharField(max_length=200, null=True, blank=True)

    url = models.CharField(max_length=400)

    sub_category = models.ForeignKey(SubCategory,  blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return unicode(self.id)

    class Meta:
        verbose_name = '11. Відеоуроки'
        verbose_name_plural = '11. Відеоуроки'


class Courses(models.Model):

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)

    name = models.CharField(max_length=200)

    password = models.CharField(max_length=400, blank=True, null=True)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '- Курси'
        verbose_name_plural = '- Курси'


class SectionCourses(models.Model):

    name = models.CharField(max_length=200)

    courses = models.ForeignKey(Courses, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '- Секція'
        verbose_name_plural = '- Секція'


class ElementSection(models.Model):

    name = models.CharField(max_length=200)

    section = models.ForeignKey(SectionCourses, null=True, blank=True, on_delete=models.SET_NULL)

    file = models.FileField(null=True, blank=True)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '- Елемент секції'
        verbose_name_plural = '- Елемент секції'
