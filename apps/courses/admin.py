
from django.contrib import admin
from apps.courses.models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'logo', 'name', 'count_sub_category',)


class SubCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'category', 'name',)


class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'sub_category', 'name',)


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('id', 'img', 'text',)


class TestAdmin(admin.ModelAdmin):
    list_display = ('id', 'topic',)


class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'question',)


class HelpTaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'test',)


class InformationTestAdmin(admin.ModelAdmin):
    list_display = ('id', 'topic',)


class HistoryTestAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'topic', 'count_correct_answer', 
                    'count_all_answer', 'percent', 'data_time',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(Test, TestAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(InformationTest, InformationTestAdmin)
admin.site.register(HistoryTest, HistoryTestAdmin)
admin.site.register(HelpTask, HelpTaskAdmin)
admin.site.register(VideoLesson)
admin.site.register(Courses)
admin.site.register(SectionCourses)
admin.site.register(ElementSection)
