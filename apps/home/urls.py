from django.conf.urls import url
from django.urls import path

from apps.home import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
]